#!/usr/bin/env sh

basedir=$(dirname $0)

xmllint --noout --dtdvalid $basedir/catalog.dtd $basedir/catalog.xml

if [ $? -eq 0 ]; then
    echo "\033[0;32mWell formed\033[0m"
else
    echo "\033[0;31mThere were errors!\033[0m"
fi
