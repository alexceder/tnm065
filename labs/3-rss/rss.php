<?php

date_default_timezone_set('Europe/Stockholm');

// Helpers
function e($string) {
    return htmlentities($string, ENT_QUOTES, 'UTF-8', false);
}

function rss_date($time = 'now') {
    return (new \DateTime($time))->format(DateTime::ATOM);
}

function db_url_to_pdo_args($options = []) {
    $url = getenv("CLEARDB_DATABASE_URL") ?: 'mysql://root:@localhost/rsslab';
    $parsed = parse_url($url);

    $db = substr($parsed['path'], 1);
    $dsn = "mysql:host={$parsed['host']};dbname={$db};charset=utf8";

    return [$dsn, $parsed['user'], $parsed['pass'], $options];
}

// Database cheese (contemporary solution)
$pdo = new \PDO(...db_url_to_pdo_args());
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$stmt = $pdo->prepare('SELECT * FROM `bookcatalog` ORDER BY `publish_date` DESC');
$stmt->execute();
$collection = $stmt->fetchAll();

// Set the header to rdf MIME type
header('Content-Type: application/xml; charset=utf-8');

?>
<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://purl.org/rss/1.0/" xmlns:dc="http://purl.org/dc/elements/1.1/">
    <channel rdf:about="http://www.itn.liu.se/">
        <title>Bokkatalog</title>
        <link>http://www.itn.liu.se/</link>
        <description>En bokkatalog.</description>
        <dc:language>en</dc:language>
        <dc:date><?php echo rss_date(); ?></dc:date>

        <dc:publisher>LiU/ITN</dc:publisher>
        <dc:creator>Alexander Cederblad</dc:creator>

        <items>
            <rdf:Seq>
<?php foreach ($collection as $item): ?>
                <rdf:li rdf:resource='<?php echo e($item->link); ?>'/>
<?php endforeach; ?>
            </rdf:Seq>
        </items>
    </channel>

<?php foreach ($collection as $item): ?>
    <item rdf:about="<?php echo e($item->link); ?>">
        <title><?php echo e($item->title); ?></title>
        <link><?php echo e($item->link); ?></link>
        <description><?php echo e($item->description); ?></description>
        <dc:creator><?php echo e($item->author); ?></dc:creator>
        <dc:date><?php echo rss_date($item->publish_date); ?></dc:date>
    </item>
<?php endforeach; ?>
</rdf:RDF>
