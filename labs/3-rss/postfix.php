<?php

$xmlstr = ob_get_contents();
ob_end_clean();

$xml = new DOMDocument;
$xml->loadXML($xmlstr);

$xsl = new DOMDocument;
$xsl->load('rss.xsl');

$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl);

header('Content-Type: text/html');
echo $proc->transformToXML($xml);
