<?php

namespace App\Database;

use Framework\DatabaseManager;

class UserRepository
{
    public function __construct(DatabaseManager $dbm)
    {
        $this->dbm = $dbm;
    }

    public function getUser($username)
    {
        $query = 'SELECT * FROM `users` WHERE `username` = :username';

        return $this->dbm->fetch($query, compact('username'));
    }

    public function storeUser($username)
    {
        $query = 'INSERT INTO `users` (`username`, `created_at`) VALUES (:username, :created_at)';

        return $this->dbm->execute($query, [
            'username' => $username,
            'created_at' => (new \DateTime)->format('Y-m-d H:i:s')
        ]);
    }

    public function getUserShows($username)
    {
        $user_id = $this->userId($username);

        $query = <<<SQL
SELECT `shows`.*, !ISNULL(`subscriptions`.`id`) as `subscribed`
FROM `shows`
LEFT OUTER JOIN `subscriptions`
ON `shows`.`id` = `subscriptions`.`show_id`
AND `subscriptions`.`user_id` = :user_id
ORDER BY `shows`.`title` DESC
SQL;

        return $this->dbm->fetchAll($query, compact('user_id'));
    }

    public function getUserUpcoming($username)
    {
        $user_id = $this->userId($username);

        $query = <<<SQL
SELECT `episodes`.*
FROM `episodes`
INNER JOIN `watchlist`
ON `episodes`.`id` = `watchlist`.`episode_id`
AND `watchlist`.`user_id` = :user_id
WHERE `episodes`.`released_at` > NOW()
ORDER BY `episodes`.`released_at` ASC
SQL;

        return $this->dbm->fetchAll($query, compact('user_id'));
    }

    public function getUserWatchlist($username)
    {
        $user_id = $this->userId($username);

        $query = <<<SQL
SELECT `episodes`.*, ISNULL(`watchlist`.`id`) as `watched`
FROM `episodes`
INNER JOIN `subscriptions`
ON `subscriptions`.`show_id` = `episodes`.`show`
AND `subscriptions`.`user_id` = :user_id
LEFT OUTER JOIN `watchlist`
ON `episodes`.`id` = `watchlist`.`episode_id`
AND `watchlist`.`user_id` = :user_id
WHERE `episodes`.`released_at` < NOW()
ORDER BY `watched` ASC, `episodes`.`released_at` DESC
SQL;

        return $this->dbm->fetchAll($query, compact('user_id'));
    }

    public function getUserSubscriptions($username)
    {
        $user_id = $this->userId($username);

        $query = <<<SQL
SELECT `shows`.*
FROM `shows`
LEFT OUTER JOIN `subscriptions`
ON `shows`.`id` = `subscriptions`.`show_id`
AND `subscriptions`.`user_id` = :user_id
ORDER BY `shows`.`title`
SQL;

        return $this->dbm->fetchAll($query, compact('user_id'));
    }

    public function removeWatchlist($username, $episode_id)
    {
        $user_id = $this->userId($username);
        $query = 'DELETE FROM `watchlist` WHERE `user_id` = :user_id AND `episode_id` = :episode_id';

        return $this->dbm->execute($query, compact('user_id', 'episode_id'));
    }

    public function storeWatchlist($username, $episode_id)
    {
        $user_id = $this->userId($username);
        $query = 'INSERT INTO `watchlist` (`user_id`, `episode_id`) VALUES (:user_id, :episode_id)';

        return $this->dbm->execute($query, compact('user_id', 'episode_id'));
    }

    public function removeSubscriptions($username, $show_id)
    {
        $user_id = $this->userId($username);

        $query = <<<SQL
DELETE `w` FROM `watchlist` `w`
INNER JOIN `episodes`
ON `episodes`.`id` = `w`.`episode_id`
WHERE `w`.`user_id` = :user_id
AND `episodes`.`show` = :show_id
SQL;
        $this->dbm->execute($query, compact('user_id', 'show_id'));

        $query = 'DELETE FROM `subscriptions` WHERE `user_id` = :user_id AND `show_id` = :show_id';
        $this->dbm->execute($query, compact('user_id', 'show_id'));

        return true;
    }

    public function storeSubscriptions($username, $show_id)
    {
        $user_id = $this->userId($username);

        // Add show subscription
        $query = 'SELECT 1 FROM `subscriptions` WHERE `user_id` = :user_id AND `show_id` = :show_id';

        if ($this->dbm->fetch($query, compact('user_id', 'show_id'))) {
            return false;
        }

        // Add show subscription
        $query = 'INSERT INTO `subscriptions` (`user_id`, `show_id`) VALUES (:user_id, :show_id)';
        $this->dbm->execute($query, compact('user_id', 'show_id'));

        // Get upcoming episodes
        $query = 'SELECT * FROM `episodes` WHERE `show` = :show_id AND `episodes`.`released_at` > NOW()';
        $upcoming = $this->dbm->fetchAll($query, compact('show_id'));

        $ids = array_map(function ($episode) {
            return $episode->id;
        }, $upcoming);

        $query_part_arr = array_map(function ($id) use ($user_id) {
            return "($user_id, $id)";
        }, $ids);

        $query_part = implode(', ', $query_part_arr);

        if (count($ids)) {
            // Add them to watchlist
            $query = 'INSERT INTO `watchlist` (`user_id`, `episode_id`) VALUES '.$query_part;
            return $this->dbm->execute($query, compact('user_id', 'show_id'));
        }

        return true;
    }

    protected function userId($username)
    {
        $obj = $this->dbm->fetch(
            'SELECT `id` FROM `users` WHERE `username` = :username',
            compact('username')
        );

        return $obj->id;
    }
}
