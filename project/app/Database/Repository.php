<?php

namespace App\Database;

use Framework\DatabaseManager;

class Repository
{
    public function __construct(DatabaseManager $dbm)
    {
        $this->dbm = $dbm;
    }

    public function getShows()
    {
        $query = 'SELECT * FROM `shows` ORDER BY `released_at`';

        return $this->dbm->fetchAll($query);
    }

    public function getShow($slug)
    {
        $query = 'SELECT * FROM `shows` WHERE `slug` = :slug ORDER BY `released_at` DESC';

        return $this->dbm->fetchAll($query, compact('slug'));
    }

    public function storeShow($data)
    {
        $query = 'INSERT INTO `shows` (`title`, `slug`, `description`, `released_at`) VALUES (:title, :slug, :description, :released_at)';

        $payload = [
            'title' => $data['title'],
            'slug' => sluggify($data['title']),
            'description' => $data['description'],
            'released_at' => (new \DateTime($data['released_at']))->format('Y-m-d H:i:s')
        ];

        $this->dbm->execute($query, $payload);
        return $payload;
    }

    public function storeShowEpisode($data)
    {
        $obj = $this->dbm->fetch(
            'SELECT `id` FROM `shows` WHERE `slug` = :slug',
            ['slug' => $data['show']]
        );
        $id = $obj->id;

        $query = 'INSERT INTO `episodes` (`title`, `slug`, `description`, `show`, `season`, `episode`, `released_at`) VALUES (:title, :slug, :description, :show, :season, :episode, :released_at)';

        $payload = [
            'title' => $data['title'],
            'slug' => sluggify($data['title']),
            'description' => $data['description'],
            'show' => $id,
            'season' => $data['season'],
            'episode' => $data['episode'],
            'released_at' => (new \DateTime($data['released_at']))->format('Y-m-d H:i:s')
        ];

        $this->dbm->execute($query, $payload);
        return $payload;
    }

    public function getEpisode($slug)
    {
        $obj = $this->dbm->fetch(
            'SELECT `id` FROM `shows` WHERE `slug` = :slug',
            compact('slug')
        );
        $id = $obj->id;

        $query = 'SELECT * FROM `episodes` WHERE `show` = :id ORDER BY `released_at` DESC';

        return $this->dbm->fetchAll($query, compact('id'));
    }

    public function getEpisodes()
    {
        $query = 'SELECT * FROM `episodes` ORDER BY `released_at`';

        return $this->dbm->fetchAll($query);
    }

    public function getShowEpisodes($slug)
    {
        $obj = $this->dbm->fetch(
            'SELECT `id` FROM `shows` WHERE `slug` = :slug',
            compact('slug')
        );

        $show = $obj->id;

        $query = 'SELECT * FROM `episodes` WHERE `show` = :show';

        return $this->dbm->fetchAll($query, compact('show'));
    }
}
