<?php

namespace App\Http;

use Framework\SessionHandler;
use App\Database\UserRepository;
use Framework\TwitterClient as Twitter;

class AuthController
{
    protected $twitter;

    protected $session;

    protected $repo;

    public function __construct(Twitter $twitter, SessionHandler $session)
    {
        $this->twitter = $twitter;
        $this->session = $session;
        $this->repo = new UserRepository(dbm());
    }

    public function auth()
    {
        $user = $this->session->get('user');

        if ($user) {
            header('Location: /');
        } else {
            $response = $this->twitter->getRequestToken();
            header("Location: {$this->twitter->getAuthenticateRedirectUrl($response['oauth_token'])}");
        }

        exit;
    }

    public function logout()
    {
        $user = $this->session->destroy();
        header("Location: /");
        exit;
    }

    public function callback()
    {
        $token = get('oauth_token');
        $verifier = get('oauth_verifier');

        $response = $this->twitter->getAccessToken($token, $verifier);
        $username = $response['screen_name'];

        // Handle user
        $user = $this->repo->getUser($username);

        if (!$user) {
            $this->repo->storeUser($username);
        }

        // Store session
        $this->session->set('user', $username);

        header('Location: /');
        exit;
    }
}
