<?php

namespace App\Http\Api;

use App\Database\Repository;
use Framework\SessionHandler;

class Controller
{
    use ApiTrait;

    protected $session;

    protected $repo;

    public function __construct(SessionHandler $session)
    {
        $this->session = $session;
        $this->repo = new Repository(dbm());
    }

    public function shows()
    {
        $records = $this->repo->getShows();
        return $this->render('shows', compact('records'));
    }

    public function show($slug)
    {
        $records = $this->repo->getShow($slug);
        return $this->render('shows', compact('records'));
    }

    public function storeShow()
    {
        return $this->repo->storeShow($this->fromJsonBody());
    }

    public function storeShowEpisode()
    {
        return $this->repo->storeShowEpisode($this->fromJsonBody());
    }

    public function removeShow($slug)
    {
        $this->repo->removeShow($slug);
        return compact('user', 'id');
    }

    public function episode($slug)
    {
        $records = $this->repo->getEpisode($slug);
        return $this->render('episodes', compact('records'));
    }

    public function episodes()
    {
        $records = $this->repo->getEpisodes();
        return $this->render('episodes', compact('records'));
    }

    public function showEpisodes($show)
    {
        $records = $this->repo->getShowEpisodes($show);
        return $this->render('episodes', compact('records'));
    }
}
