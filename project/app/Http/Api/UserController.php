<?php

namespace App\Http\Api;

use App\Database\UserRepository;
use Framework\SessionHandler;

class UserController
{
    use ApiTrait;

    protected $repo;

    protected $session;

    public function __construct(SessionHandler $session)
    {
        $this->session = $session;
        $this->repo = new UserRepository(dbm());
    }

    public function user($user)
    {
        if ($this->session->get('user')) {
            $record = $this->repo->getUser($user);
            return $this->render('user', compact('record'));
        }

        http_response_code(422);
        return ['message' => 'This is not available to you.'];
    }

    public function auth()
    {
        return $this->user($this->session->get('user'));
    }

    public function shows($user)
    {
        $records = $this->repo->getUserShows($user);
        return $this->render('shows', compact('records'));
    }

    public function watchlist($user)
    {
        $records = $this->repo->getUserWatchlist($user);
        return $this->render('episodes', compact('records'));
    }

    public function removeWatchlist($user, $id)
    {
        $this->repo->removeWatchlist($user, $id);
        return compact('user', 'id');
    }

    public function storeWatchlist($user)
    {
        $id = $this->fromJsonBody('episode_id');

        $this->repo->storeWatchlist($user, $id);
        return compact('user', 'id');
    }

    public function upcoming($user)
    {
        $records = $this->repo->getUserUpcoming($user);
        return $this->render('episodes', compact('records'));
    }

    public function subscriptions($user)
    {
        $records = $this->repo->getUserSubscriptions($user);
        return $this->render('shows', compact('records'));
    }

    public function removeSubscriptions($user, $id)
    {
        $this->repo->removeSubscriptions($user, $id);
        return compact('user', 'id');
    }

    public function storeSubscriptions($user)
    {
        $id = $this->fromJsonBody('show_id');

        if ($this->repo->storeSubscriptions($user, $id)) {
            return compact('user', 'id');
        }

        http_response_code(422);
        return ['message' => 'This subscription already exists.'];
    }
}
