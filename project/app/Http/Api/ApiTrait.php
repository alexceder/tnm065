<?php

namespace App\Http\Api;

trait ApiTrait
{
    protected function render($template, $data)
    {
        return template($template)
            ->transform(get('type') ?? 'json')
            ->mime($this->id2mime(get('type')))
            ->render($data);
    }

    protected function id2mime($id)
    {
        $map = [
            'atom' => 'application/atom+xml',
            'html' => 'text/html',
            'json' => 'application/json',
            'raw' => 'application/xml',
            'rss' => 'application/rss+xml',
            'xml' => 'application/xml',
        ];

        return isset($map[$id]) ? $map[$id] : $map['json'];
    }

    protected function fromJsonBody($key = null)
    {
        $request = \Framework\Http\Request::createFromGlobals();
        $body = json_decode($request->getBody(), true);

        if (null === $key) {
            return $body;
        }

        return $body[$key];
    }
}
