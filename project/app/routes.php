<?php

$router->register('GET', '/', function () {
    return template('app')->render();
});


// Auth

$router->register('GET', '/auth',          'AuthController@auth');
$router->register('GET', '/auth/logout',   'AuthController@logout');
$router->register('GET', '/auth/callback', 'AuthController@callback');


// Basic Resources

$router->register('GET', '/api/shows',               'Api\\Controller@shows');
$router->register('POST', '/api/shows',              'Api\\Controller@storeShow');
$router->register('GET', '/api/shows/:id',           'Api\\Controller@show');
$router->register('DELETE', '/api/shows/:id',        'Api\\Controller@show');
$router->register('GET', '/api/shows/:id/episodes',  'Api\\Controller@showEpisodes');
$router->register('POST', '/api/shows/:id/episodes', 'Api\\Controller@storeShowEpisode');

$router->register('GET', '/api/episodes', 'Api\\Controller@episodes');


// User

$router->register('GET', '/api/auth', 'Api\\UserController@auth');

$router->register('GET', '/api/users/:user', 'Api\\UserController@user');

$router->register('GET', '/api/users/:user/shows', 'Api\\UserController@shows');

$router->register('GET',    '/api/users/:user/subscriptions',     'Api\\UserController@subscriptions');
$router->register('POST',   '/api/users/:user/subscriptions',     'Api\\UserController@storeSubscriptions');
$router->register('DELETE', '/api/users/:user/subscriptions/:id', 'Api\\UserController@removeSubscriptions');

$router->register('GET',    '/api/users/:user/watchlist',     'Api\\UserController@watchlist');
$router->register('POST',   '/api/users/:user/watchlist',     'Api\\UserController@storeWatchlist');
$router->register('DELETE', '/api/users/:user/watchlist/:id', 'Api\\UserController@removeWatchlist');

$router->register('GET',    '/api/users/:user/upcoming',     'Api\\UserController@upcoming');


// Images

$router->register('GET', '/api/shows/:show/image', function ($show) {
    if (!stristr($show, '..')) {
        if ('.jpg' !== substr($show, -4)) {
            $show = $show.'.jpg';
        }

        $path = __DIR__.'/../public/images/'.$show;

        if (is_file($path)) {
            $ua = $_SERVER['HTTP_USER_AGENT'];
            preg_match("/iPhone|Android|iPad|iPod|webOS/", $ua, $matches);

            if (count($matches)) {
                $org_size = getimagesize($path);

                $width = $org_size[0]/3;
                $height = $org_size[1]/3;

                $original = imagecreatefromjpeg($path);

                $image = imagecreatetruecolor($width, $height);
                imagecopyresampled($image, $original, 0, 0, 0, 0, $width, $height, $org_size[0], $org_size[1]);

                header('Content-Type: image/jpeg');
                imagejpeg($image, null, 75);
                imagedestroy($image);
                exit;
            }

            header('Content-Type: image/jpeg');
            return file_get_contents($path);
        }
    }
});


// Definitions

$router->register('GET', '/definitions/:filename', function ($filename) {
    if (!stristr($filename, '..')) {
        if ('.dtd' !== substr($filename, -4)) {
            $filename = $filename.'.dtd';
        }

        $path = __DIR__.'/../definitions/'.$filename;

        if (is_file($path)) {
            return file_get_contents($path);
        }
    }
});
