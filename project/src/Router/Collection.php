<?php

namespace Framework\Router;

class Collection implements \IteratorAggregate
{
    private $routes = [];

    private $prefix = '';

    public function add($name, Route $route)
    {
        if ('' !== $this->prefix) {
            $route->setPath('/'.$this->prefix.$route->getPath());
        }

        if ('' === $this->prefix) {
            $key = $name;
        } else {
            $key = str_replace('/', '.', $this->prefix).'.'.$name;
        }

        $this->routes[$route->getMethod().':'.$key] = $route;
    }

    public function addCollection(Collection $routes)
    {
        $this->routes = array_merge($this->routes, $routes->all());
    }

    public function setPrefix($prefix)
    {
        foreach ($this->routes as $route) {
            $route->setPath('/'.$prefix.$route->getPath());
        }

        $this->prefix = $prefix;
    }

    public function all()
    {
        return $this->routes;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->routes);
    }
}
