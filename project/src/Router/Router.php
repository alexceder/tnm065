<?php

namespace Framework\Router;

use Framework\Http\Request;
use Framework\Router\Route;
use Framework\Router\Collection;
use Framework\Http\NotFoundException;

class Router
{
    protected $routes;

    public function __construct()
    {
        $this->routes = new Collection;
    }

    public function register($method, $pattern, $action)
    {
        $this->routes->add($pattern, new Route($method, $pattern, $action));
    }

    public function resolve(Request $request)
    {
        foreach ($this->routes as $route) {
            if ($route->getMethod() !== $request->getMethod()) {
                continue;
            }

            if ($route->getPath() === $request->getBaseUri()) {
                return $route();
            } elseif (preg_match_all('#:[^\/]+#', $route->getPath(), $matches)) {
                $matches = $matches[0]; // yolo

                $regex = $route->compile($matches);

                if (preg_match_all($regex, $request->getBaseUri(), $params)) {
                    $args = [];
                    foreach ($matches as $match) {
                        $args[] = $params[substr($match, 1)][0];
                    }

                    return $route(...$args);
                }
            }
        }

        throw new NotFoundException;
    }
}
