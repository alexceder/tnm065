<?php

namespace Framework\Router;

class Route
{
    protected $method;

    protected $path;

    protected $action;

    function __construct($method, $path, $action)
    {
        $this->method = $method;
        $this->path = $path;
        $this->action = $action;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function compile(array $matches) {
        $accum_regex = $this->path;
        $replacements = [];
        $patterns = [];

        foreach ($matches as $match) {
            $replacements[] = '(?P<'.substr($match, 1).'>[^\/]+)';
            $patterns[] = '#'.$match.'#';
        }

        return '#^'.preg_replace($patterns, $replacements, $accum_regex, 1).'$#';
    }

    protected function resolveDependencies($class)
    {
        $reflection = new \ReflectionClass($class);
        $constructor = $reflection->getConstructor();
        $deps = [];

        if (!is_null($constructor)) {
            foreach ($constructor->getParameters() as $param) {
                if ($param = $param->getClass()) {
                    $deps[] = new $param->name;
                }
            }
        }

        return $deps;
    }

    public function __invoke(...$args) {

        if ($this->action instanceof \Closure) {
            return call_user_func_array($this->action, $args);
        }

        $action_arr = explode('@', $this->action);
        $class = 'App\\Http\\'.$action_arr[0];
        $method = $action_arr[1];

        $deps = $this->resolveDependencies($class);
        $controller = new $class(...$deps);

        return $controller->{$method}(...$args);
    }
}
