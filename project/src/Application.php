<?php

namespace Framework;

use Framework\Http\Request;
use Framework\Router\Router;

class Application
{
    protected $router;

    public function __construct()
    {
        $this->router = new Router;

        $this->setupPhp();
        $this->setupErrorHandling();

        $this->registerRoutes();
    }

    public function boot()
    {
        $request = Request::createFromGlobals();

        $response = $this->router->resolve($request);

        if (is_array($response)) {
            $response = json_encode($response);
        }

        echo $response;
    }

    private function registerRoutes()
    {
        call_user_func(function ($router) {
            require __DIR__.'/../app/routes.php';
        }, $this->router);
    }

    private function setupPhp()
    {
        mb_internal_encoding('UTF-8');
        date_default_timezone_set('Europe/Stockholm');
    }

    private function setupErrorHandling()
    {
        if (false) {
            //
        } else {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
    }
}
