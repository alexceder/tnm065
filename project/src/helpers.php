<?php


if (!function_exists('e')) {
    /**
     * Convert applicable characters to HTML characters.
     *
     * @param  mixed  $string
     * @return string
     */
    function e($string) {
        return htmlentities($string, ENT_QUOTES, 'UTF-8', false);
    }
}

if (!function_exists('iso_date')) {
    /**
     * Return ISO-8601 formatted date.
     *
     * DateTime::ISO8601 is actually not conforming to the standard.
     * Further reading: http://php.net/manual/en/class.datetime.php#datetime.constants.atom
     *
     * @param  mixed  $time
     * @return string
     */
    function rss_date($time = 'now') {
        return (new DateTime($time))->format(DateTime::ATOM);
    }
}

if (!function_exists('dbm')) {
    /**
     * @return \Framework\DataManager
     */
    function dbm()
    {
        static $repo = null;

        if (null === $repo) {
            $url = getenv("CLEARDB_DATABASE_URL")
                ?: 'mysql://root:@localhost/teve';
            $parsed = parse_url($url);

            $repo = new Framework\DatabaseManager(
                substr($parsed['path'], 1),
                $parsed['host'],
                $parsed['user'],
                $parsed['pass']
            );
        }

        return $repo;
    }
}

if (!function_exists('template')) {
    /**
     * @param  mixed  $name
     * @return \Framework\Template
     */
    function template($name)
    {
        return new Framework\Template($name);
    }
}

if (!function_exists('session')) {
    /**
     * @param  mixed  $key
     * @return string|null
     */
    function session($key)
    {
        return (new Framework\SessionHandler)->get($key);
    }
}

if (!function_exists('get')) {
    /**
     * @param  mixed  $key
     * @return string|null
     */
    function get($key)
    {
        return !empty($_GET[$key]) ? $_GET[$key] : null;
    }
}

if (!function_exists('url')) {
    /**
     * @param  mixed  $key
     * @return string|null
     */
    function url($path = null)
    {
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'
            ? 'https' : 'http';

        $host = $_SERVER['HTTP_HOST'] ?? null;
        $url = $protocol.'://'.$host;

        if (null === $path || '' === trim($path, '/')) {
            return $url;
        }

        return $url.'/'.trim($path, '/');
    }
}

if (!function_exists('sluggify')) {
    /**
     * @param  string  $value
     * @return string
     */
    function sluggify($value)
    {
        return trim(str_replace('.', '', (str_replace(' ', '-', mb_strtolower($value)))));
    }
}
