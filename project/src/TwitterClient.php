<?php

namespace Framework;

class TwitterClient
{
    protected $base = 'https://api.twitter.com';

    protected $secret_key = 'jdI6O5yfs0cXgWbLRINFs9baHLaczeH7HEP8vJOjxPm2ji48Ij';

    protected $consumer_key = 'Y98aLfzLAUQTQdpSuK0vz49f9';

    public function getRequestToken()
    {
        $method = 'POST';
        $endpoint = '/oauth/request_token';
        //$callback = 'http://localhost:8888/auth/twitter';
        $url = $this->base.$endpoint;

        $components = [
            //'oauth_callback' => rawurlencode($callback),
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => bin2hex(random_bytes(16)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        ];

        $components['oauth_signature'] = $this->createSignature(
            $components,
            $method,
            $url
        );

        return $this->makeRequest($url, $components);
    }

    public function getAccessToken($token, $verifier)
    {
        $method = 'POST';
        $endpoint = '/oauth/access_token';
        //$callback = 'http://localhost:8888/auth/twitter';
        $url = $this->base.$endpoint;

        $components = [
            //'oauth_callback' => rawurlencode($callback),
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => bin2hex(random_bytes(16)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_token' => $token,
            'oauth_version' => '1.0'
        ];

        $components['oauth_signature'] = $this->createSignature(
            $components,
            $method,
            $url
        );

        $body = ['oauth_verifier' => $verifier];

        return $this->makeRequest($url, $components, $body);
    }

    public function getAuthenticateRedirectUrl($token)
    {
        return 'https://api.twitter.com/oauth/authenticate?oauth_token='.$token;
    }

    protected function makeRequest($url, $components, array $body = null)
    {
        $header = $this->buildHeaderString($components);

        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, [$header]);

        if (null !== $body) {
            curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($body));
        }

        $response = curl_exec($handle);
        curl_close($handle);

        parse_str($response, $response_arr);

        return $response_arr;
    }

    protected function createSignature($components, $method, $url)
    {
        ksort($components);
        $query = http_build_query($components);

        $data = $method.'&'.rawurlencode($url).'&'.rawurlencode($query);
        $signature = hash_hmac('sha1', $data, rawurlencode($this->secret_key).'&', true);

        return rawurlencode(base64_encode($signature));
    }

    protected function buildHeaderString($components)
    {
        ksort($components);

        $header = 'Authorization: OAuth ';

        foreach ($components as $key => $component) {
            $header .= $key.'="'.$component.'", ';
        }

        return rtrim($header, ', ');
    }
}
