<?php

namespace Framework;

use PDO;

class DatabaseManager
{
    public function __construct($database, $host = 'localhost', $username = 'root', $password = '')
    {
        $this->pdo = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $username, $password);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function fetchAll($query, $parameters = [])
    {
        return $this->get($query, $parameters)->fetchAll();
    }

    public function fetch($query, $parameters = [])
    {
        return $this->get($query, $parameters)->fetch();
    }

    public function execute($query, $parameters = [])
    {
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute($parameters);
    }

    protected function get($query, $parameters = [])
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($parameters);
        return $stmt;
    }
}
