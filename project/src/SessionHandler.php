<?php

namespace Framework;

class SessionHandler
{
    static protected $started = false;

    public function start()
    {
        if (!self::$started) {
            session_start();
            self::$started = true;
        }
    }

    public function destroy()
    {
        $this->start();

        $_SESSION = [];

        if (ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();

            setcookie(
                session_name(),
                '',
                time()-3600,
                $params['path'],
                $params['domain'],
                $params['secure'],
                $params['httponly']
            );
        }

        session_unset();
        session_destroy();
    }

    public function set($key, $value)
    {
        if (!self::$started) {
            $this->start();
        }

        $_SESSION[$key] = $value;
    }

    public function get($key)
    {
        if (!self::$started) {
            $this->start();
        }

        return $_SESSION[$key] ?? null;
    }
}
