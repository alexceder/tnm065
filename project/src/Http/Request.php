<?php

namespace Framework\Http;

class Request
{
    public static function createFromGlobals()
    {
        return new self;
    }

    public function getBaseUri()
    {
        $uri = $this->server('REQUEST_URI');

        if (false !== ($pos = strpos($uri, '?'))) {
            return substr($uri, 0, $pos);
        }

        return $uri;
    }

    public function getBody()
    {
        return file_get_contents('php://input');
    }

    public function getMethod()
    {
        return $this->server('REQUEST_METHOD');
    }

    public function get($key = null)
    {
        return $_GET[$key] ?? null;
    }

    public function server($key = null)
    {
        return $_SERVER[$key] ?? null;
    }
}
