<?php

namespace Framework\Http;

class NotFoundException extends \Exception
{
    function __construct()
    {
        http_response_code(404);
    }
}
