<?php

namespace Framework;

class Template
{
    protected $name;

    protected $mime = null;

    protected $xslProcessor = null;
    
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function mime($mime = null)
    {
        $this->mime = $mime;

        return $this;
    }

    public function transform($name = null)
    {
        if (!('raw' === $name || null === $name)) {
            $xsl = new \DOMDocument;
            $xsl->load(__DIR__."/../transforms/{$name}.xsl");

            $this->xslProcessor = new \XSLTProcessor;
            $this->xslProcessor->importStyleSheet($xsl);
        }

        return $this;
    }

    public function render($data = [])
    {
        extract($data);
        unset($data);

        ob_start(); 

        require __DIR__.'/../templates/'.$this->name.'.php';

        $content = ob_get_clean();

        if (null !== $this->mime) {
            header("Content-Type: {$this->mime}; charset=utf-8");
        }

        if (null !== $this->xslProcessor) {
            $xml = new \DOMDocument;
            $xml->loadXML($content);

            return $this->xslProcessor->transformToXML($xml);
        }

        return $content;
    }
}
