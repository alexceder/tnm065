/* SHOWS
---------------------- */

DROP TABLE IF EXISTS `shows`;

CREATE TABLE `shows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `released_at` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `shows` WRITE;

INSERT INTO `shows` (`title`, `slug`, `description`, `released_at`)
VALUES
    ('Game of Thrones', 'game-of-thrones', 'An adaptation of author George R.R. Martin\'s "A Song of Ice and Fire" medieval fantasies about power struggles among the Seven Kingdoms of Westeros.', '2011-04-17 00:00:00'),
    ('The Flash', 'the-flash', 'Barry Allen wakes up 9 months after he was struck by lightning and discovers that the bolt gave him the power of super speed. With his new team and powers, Barry becomes "The Flash" and fights crime in Central City.', '2014-10-07 00:00:00'),
    ('Arrow', 'arrow', 'Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.', '2012-10-10 00:00:00');

UNLOCK TABLES;


/* EPISODES
---------------------- */

DROP TABLE IF EXISTS `episodes`;

CREATE TABLE `episodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `show` int(10) NOT NULL,
  `season` int(10) NOT NULL,
  `episode` int(10) NOT NULL,
  `released_at` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `episodes` WRITE;

INSERT INTO `episodes` (`title`, `slug`, `description`, `show`, `season`, `episode`, `released_at`)
VALUES
    ('The Wars to Come', 'the-wars-to-come', 'Cersei and Jaime adjust to a world without Tywin. Tyrion and Varys arrive at Pentos. In Meereen, a new enemy emerges. Jon is caught between', 1, 5, 1, '2015-04-12 21:00:00'),
    ('The House of Black and White', 'the-house-of-black-and-white', 'Arya arrives in Braavos. Jamie takes on a secret mission. Ellaria Sand seeks revenge for Oberyn\'s death. Stannis makes Jon a generous offer as the Night\'s Watch elects a new Lord Commander. Daenerys is faced with a difficult decision', 1, 5, 2, '2015-04-19 21:00:00'),
    ('High Sparrow', 'high-sparrow', 'Tommen and Margaery wed. Arya has trouble adapting to her new life. Littlefinger reveals his plans to Sansa. Jon gives his first orders as Lord Commander. Tyrion and Varys arrive in Volantis', 1, 5, 3, '2015-04-26 21:00:00'),
    ('Sons of the Harpy', 'sons-of-the-harpy', 'Jorah Mormont sets sail alongside his prisoner, Tyrion. Cersei makes a move against the Tyrells. Jaime and Bronn sneak into Dorne. Ellaria and the Sand Snakes make their plans. Melisandre tempts Jon. The Harpies attack', 1, 5, 4, '2015-05-03 21:00:00'),
    ('Kill the Boy', 'kill-the-boy', 'Daenerys arrests the heads of Meereen\'s great families. Jon makes a difficult decision. Theon is forced to face Sansa. Stannis rides south. Tyrion and Jorah enter the ruins of Old Valyria', 1, 5, 5, '2015-05-10 21:00:00'),
    ('Unbowed, Unbent, Unbroken', 'unbowed-unbent-unbroken', 'Arya is put to the test. Tyrion and Jorah are captured by slavers. Loras Tyrell is judged by the Sparrows. Jaime and Bronn face the Sand Snakes. Sansa marries Ramsay Bolton', 1, 5, 6, '2015-05-17 21:00:00'),
    ('The Gift', 'the-gift', 'Jon heads east as trouble begins to stir for Sam and Gilly at Castle Black. Sansa asks Theon for help. Tyrion and Jorah are sold as slaves. Cersei savors her triumph over the Tyrells as new plots are developed in the shadows', 1, 5, 7, '2015-05-24 21:00:00'),
    ('Hardhome', 'hardhome', 'Tyrion advises Daenerys. Sansa forces Theon to tell her a secret. Cersei remains stubborn. Arya meets her first target. Jon and Tormund meet with the Wildling Elders', 1, 5, 8, '2015-05-31 21:00:00'),
    ('The Dance of Dragons', 'the-dance-of-dragons', 'Jon and the Wildlings return to Castle Black. Jamie meets with Doran Martell. Stannis makes a hard choice. Arya runs into Ser Meryn Trant. Daenerys attends the grand reopening of the fighting pits', 1, 5, 9, '2015-06-07 21:00:00'),
    ('Mother\'s Mercy', 'mothers-mercy', 'Stannis arrives at Winterfell. Tyrion runs Meereen as Daario and Jorah go after Daenerys. Jaime and Myrcella leave Dorne. Jon sends Sam and Gilly to Oldtown. Arya challenges the many faced god. Cersei confesses her sins', 1, 5, 10, '2015-06-14 21:00:00'),

    ('Flash of Two Worlds', 'the-man-who-saved-central-city', 'A mysterious man has a warning about an evil speedster intent on destroying The Flash; a determined officer wants to join Joe\'s meta-human task force.', 2, 2, 2, '2015-10-13 20:00:00'),
    ('Family of Rogues', 'family-of-rogues', 'Barry and the team ally with the kidnapped Captain Cold\'s sister, but Barry feels duped with he learns Snart is working on something with his father; Joe faces a difficult decision.', 2, 2, 3, '2015-10-20 20:00:00'),
    ('The Fury of Firestorm', 'the-fury-of-firestom', 'Barry and the team look for another Firestorm match for Dr. Stein. When the team meets Jefferson Jackson, Caitlin has her reservations about whether Jax is the right match for Dr. Stein. Iris surprises Joe while Barry and Patty grow closer.', 2, 2, 4, '2015-10-27 20:00:00'),
    ('The Darkness and the Light', 'the-darkness-and-the-light', 'Barry learns a new breacher, Dr. Light, has come through the portal and sets off to capture her. Jay tells Barry that Dr. Light was not a threat on Earth-2 and that Barry can reason with her. However, during a fight with The Flash, she blinds him and drops some shocking news about Zoom. Meanwhile, Barry and Patty go out on a date.', 2, 2, 5, '2015-11-03 20:00:00'),
    ('Enter Zoom', 'enter-zoom', 'Barry and his team plan to trap Zoom with Linda\'s help while Joe is against it.', 2, 2, 6, '2015-11-10 20:00:00'),
    ('Gorilla Warfare', 'gorilla-warfare', 'Barry races to rescue Caitlyn when Grodd kidnaps her; Cisco plans a date with the new barista at Jitters; Patty thinks Barry is hiding something.', 2, 2, 7, '2015-11-17 20:00:00'),
    ('Legends of Today', 'legends-of-today', 'When Vandal Savage attacks Kendra Saunders, Barry takes Kendra to Starling City, seeking Oliver\'s protection; Harrison asks Jay to test a new serum to make Barry run faster.', 2, 2, 8, '2015-12-01 20:00:00'),
    ('Running to Stand Still', 'running-to-stand-still', 'Weather Wizard returns to break Captain Cold and The Trickster out of Iron Heights, but Barry is determined to not let that happen. Meanwhile, Joe and Iris meet Wally West. Harrison Wells accepts to work with Zoom against Barry.', 2, 2, 9, '2015-12-08 20:00:00'),
    ('Potential Energy', 'potential-energy', 'Barry and the Star Labs team hunt down The Turtle who can slow down time especially anyone with speed. Barry faces a choice to whatever or not Patty should learn his secret identity especially after Zoom returns to Earth 1 to kidnap her.', 2, 2, 10, '2016-01-19 20:00:00'),
    ('The Reverse-Flash Returns', 'the-reverse-flash-returns', 'Barry and the team don\'t believe Cisco when he reveals he got a vibe of Eobard Thawne. Eobard attacks Mercury Labs and Tina McGee confirms that he has returned. Iris and Francine share a nice moment that brings Iris closer to her brother Wally.', 2, 2, 11, '2016-01-26 20:00:00'),

    ('Green Arrow', 'green-arrow', 'A new enemy lures Oliver Queen back to Starling City and forces him to put on the hood once more.', 3, 4, 1, '2015-10-07 20:00:00'),
    ('The Candidate', 'the-candidate', 'Oliver and Thea are concerned when a family friend plans to run for mayor; Thea starts to exhibit side effects from the Lazarus Pit; Felicity asks an employee for help with a business decision.', 3, 4, 2, '2015-10-14 20:00:00'),
    ('Restoration', 'restoration', 'The growing tension between Oliver and Diggle puts both their lives at risk when they go after Damien Darhk and a H.I.V.E. deployed meta-human. Meanwhile, Laurel talks Thea into returning to Nanda Parbat to ask her father to put Sara into the Lazarus Pit. However, Laurel is surprised when Nyssa refuses to do it.', 3, 4, 3, '2015-10-21 20:00:00'),
    ('Beyond Redemption', 'beyond-redemption', 'Laurel must deal with the repercussions of taking Sara to Nanda Parbat. Meanwhile, Oliver asks Captain Lance for a favor and while he\'s not surprised at the response, he is surprised at what he finds out next.', 3, 4, 4, '2015-10-28 20:00:00'),
    ('Haunted', 'haunted', 'Oliver calls in a favor from old friend John Constantine when Sara takes a turn for the worse.', 3, 4, 5, '2015-11-04 20:00:00'),
    ('Lost Souls', 'lost-souls', 'Felicity is frantic when she learns that Ray is alive and being held by Damien Darhk. Felicity\'s guilt over not finding Ray sooner causes tension between her and Oliver.', 3, 4, 6, '2015-11-11 20:00:00'),
    ('Brotherhood', 'brotherhood', 'A shocking revelation complicates the battle between Team Arrow and Damien Darhk; Thea loses control in front of Alex.', 3, 4, 7, '2015-11-18 20:00:00'),
    ('Legends of Yesterday', 'legends-of-yesterday', 'Oliver and Barry hide Kendra and Carter in a remote location while they try to figure out how to defeat Vandal Savage; Felicity, Thea, Diggle and Laurel work to come up with a weapon to destroy Vandal.', 3, 4, 8, '2015-12-02 20:00:00'),
    ('Dark Waters', 'dark-waters', 'Oliver moves against HIVE; Malcolm goes to see Thea, leaving her with a warning; Damien Darhk retaliates at Oliver\'s holiday party.', 3, 4, 9, '2015-12-09 20:00:00'),
    ('Blood Debts', 'blood-debts', '', 3, 4, 10, '2016-01-20 20:00:00'),
    ('A.W.O.L.', 'awol', 'An agent of Shadowspire who was an enemy from Diggle and Andy\'s wartime past shows up to Star City. Diggle must learn to trust Andy but he got more than he wanted about their shared time from war. Oliver however must learn a new way of life when Felicity is now in a wheelchair and receives her codename.', 3, 4, 11, '2016-01-27 20:00:00'),
    ('Unchained', 'unchained', '', 3, 4, 12, '2016-02-03 20:00:00'),
    ('Sins of the Father', 'sins-of-the-father', '', 3, 4, 13, '2016-02-10 20:00:00'),
    ('Code of Silence', 'code-of-silence', '', 3, 4, 14, '2016-02-17 20:00:00'),
    ('Taken', 'taken', '', 3, 4, 15, '2016-02-24 20:00:00');



UNLOCK TABLES;


/* USERS
---------------------- */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NULL,
  `created_at` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;

INSERT INTO `users` (`username`, `email`, `created_at`)
VALUES
    ('alexceder', 'test@example.com', '2015-01-01 13:00:00');

UNLOCK TABLES;


/* SUBSCRIPTIONS
---------------------- */

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE `subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `show_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `subscriptions` WRITE;

INSERT INTO `subscriptions` (`show_id`, `user_id`, `created_at`)
VALUES
    (1, 1, '2015-01-01 14:00:00');

UNLOCK TABLES;


/* VIEWS
---------------------- */

DROP TABLE IF EXISTS `watchlist`;

CREATE TABLE `watchlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `episode_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `watchlist` WRITE;

INSERT INTO `watchlist` (`episode_id`, `user_id`)
VALUES
    (8, 1),
    (9, 1),
    (10, 1);

UNLOCK TABLES;
