<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Keep up with your television shows &mdash; teve.io</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css"/>
    <style>
    body {
        margin-bottom: 10rem;
    }

    h1,
    h2 {
        margin-bottom: 2rem;
        margin-top: 2rem;
    }

    .search-jumbotron {
        margin-bottom: 2rem;
        padding: 2rem;
    }

    .watched {
        opacity: .3;
    }
    </style>
    </head>
<body>

<div id="app">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <nav class="navbar navbar-light bg-faded">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a class="nav-link" v-link="{ path: '/' }">Watchlist</a></li>
                        <li class="nav-item"><a class="nav-link" v-link="{ path: '/upcoming' }">Upcoming</a></li>
                        <li class="nav-item"><a class="nav-link" v-link="{ path: '/shows' }">Shows</a></li>
                    </ul>
                    <ul id="auth" class="nav navbar-nav pull-xs-right">
                        <li class="nav-item"><a class="nav-link" href="#"><strong><auth-user></auth-user></strong></a></li>
<?php if (!session('user')): ?>
                        <li class="nav-item"><a class="nav-link" href="<?php echo url('auth'); ?>">Sign in with Twitter</a></li>
<?php else: ?>
                        <li class="nav-item"><a class="nav-link" href="<?php echo url('auth/logout'); ?>">Logout</a></li>
<?php endif; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <router-view></router-view>
            </div>
        </div>
    </div>
</div>

<template id="watchlist">
    <h1>{{ title }} <small class="text-muted">{{ numberUnwatched }}</small></h1>
    <div v-for="episode in episodes" v-bind:class="{ 'watched': episode.watched }">
        <h5>{{ episode.title }} <small class="text-muted">{{ episode.released_at | from_now }}</small></h5>
        <p><strong>Season {{ episode.season }}, Episode {{ episode.number }}</strong></p>
        <p>{{ episode.description }}</p>
        <button type="button" class="btn btn-primary"
            v-show="toggleable"
            v-bind:class="[ episode.watched ? 'btn-secondary' : 'btn-primary' ]"
            @click="toggleWatched($index)"
        >Mark as {{ episode.watched ? 'Unwatched' : 'Watched' }}</button>
        <hr>
    </div>
</template>

<template id="shows">
    <h1>Shows <small class="text-muted">{{ shows.length }}</small></h1>

    <form class="search-jumbotron bg-faded">
        <input type="search" class="form-control" placeholder="Search the TV show index.." v-model="searchQuery">
    </form>

    <div v-for="show in shows | filterBy searchQuery in 'title'">
        <h5><a v-link="{ name: 'show', params: { show: show.slug} }">{{ show.title }}</a> <small class="text-muted">{{ show.released_at | from_now }}</small></h5>
        <p>{{ show.description }}</p>
        <a class="btn btn-primary"
            v-bind:class="[ show.subscribed ? 'btn-danger' : 'btn-primary' ]"
            @click="toggleSubscribed($index)"
        >{{ show.subscribed ? 'Unsubscribe' : 'Subscribe' }}</a>
        <hr>
    </div>

    <div>
        <a v-link="'shows/new'" class="btn btn-primary">Create show</a>
    </div>
</template>

<template id="shows-new">
    <h1>Create show</h1>
    <form v-on:submit.prevent>
        <fieldset class="form-group">
            <label for="form-title">Title</label>
            <input type="text" class="form-control" id="form-title" v-model="form.title">

            <label for="form-date">Release date</label>
            <input type="text" class="form-control" id="form-date" v-model="form.released_at">

            <label for="form-date">Description</label>
            <textarea class="form-control" id="form-date" v-model="form.description"></textarea>
        </fieldset>

        <fieldset class="form-group">
            <button class="btn btn-primary" @click="create()">Submit</button>
        </fieldset>
    </form>
</template>

<template id="shows-show">
    <h1>{{ show.title }} <small class="text-muted">{{ show.released_at | from_now }}</small></h1>
    <p>{{ show.description }}</p>

    <h2>Latest episodes</h2>
    <div v-for="episode in episodes">
        <h5>{{ episode.title }} <small class="text-muted">{{ episode.released_at | from_now }}</small></h5>
        <p><strong>Season {{ episode.season }}, Episode {{ episode.number }}</strong></p>
        <p>{{ episode.description }}</p>
        <hr>
    </div>

    <div>
        <a v-link="{ name: 'newShowEpisode', params: { show: $route.params.show } }" class="btn btn-primary">Create episode</a>
    </div>
</template>

<template id="shows-show-episodes-new">
    <h1>Create <em>{{ show.title }}</em> episode</h1>
    <form v-on:submit.prevent>
        <fieldset class="form-group">
            <label for="form-title">Title</label>
            <input type="text" class="form-control" id="form-title" v-model="form.title">

            <label for="form-date">Release date</label>
            <input type="text" class="form-control" id="form-date" v-model="form.released_at">

            <label for="form-season">Season</label>
            <input type="text" class="form-control" id="form-season" v-model="form.season">

            <label for="form-date">Episode</label>
            <input type="text" class="form-control" id="form-episode" v-model="form.episode">

            <label for="form-description">Description</label>
            <textarea class="form-control" id="form-description" v-model="form.description"></textarea>
        </fieldset>

        <fieldset class="form-group">
            <button class="btn btn-primary" @click="create()">Submit</button>
        </fieldset>
    </form>
</template>

<script src="//cdnjs.cloudflare.com/ajax/libs/vue/1.0.13/vue.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/vue-router/0.7.7/vue-router.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/vue-resource/0.6.0/vue-resource.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.js"></script>
<script src="/js/app.js"></script>
</html>
