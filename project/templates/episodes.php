<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; ?>
<!DOCTYPE items SYSTEM "<?php echo url('definitions/episodes'); ?>">
<episodes>
<?php foreach ($records as $record): ?>
    <episode>
        <id><?php echo e($record->id); ?></id>
        <title><?php echo e($record->title); ?></title>
        <slug><?php echo e($record->slug); ?></slug>
        <link><?php echo e(url("episodes/{$record->slug}")); ?></link>
        <description><?php echo e($record->description); ?></description>
        <show><?php echo e($record->show); ?></show>
        <season><?php echo e($record->season); ?></season>
        <number><?php echo e($record->episode); ?></number>
        <released_at><?php echo rss_date($record->released_at); ?></released_at>
<?php if (isset($record->watched)): ?>
        <watched><?php echo $record->watched ? 'true' : 'false'; ?></watched>
<?php endif; ?>
    </episode>
<?php endforeach; ?>
</episodes>
