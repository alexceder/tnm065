<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; ?>
<!DOCTYPE items SYSTEM "<?php echo url('definitions/shows'); ?>">
<shows>
<?php foreach ($records as $record): ?>
    <show>
        <id><?php echo e($record->id); ?></id>
        <title><?php echo e($record->title); ?></title>
        <slug><?php echo e($record->slug); ?></slug>
        <link><?php echo e(url("shows/{$record->slug}")); ?></link>
        <description><?php echo e($record->description); ?></description>
        <released_at><?php echo rss_date($record->released_at); ?></released_at>
<?php if (isset($record->subscribed)): ?>
        <subscribed><?php echo $record->subscribed ? 'true' : 'false'; ?></subscribed>
<?php endif; ?>
    </show>
<?php endforeach; ?>
</shows>
