<?php echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"; ?>
<!DOCTYPE items SYSTEM "<?php echo url('definitions/users.dtd'); ?>">
<user>
    <username><?php echo e($record->username); ?></username>
    <email><?php echo e($record->email); ?></email>
    <created_at><?php echo rss_date($record->created_at); ?></created_at>
</user>
