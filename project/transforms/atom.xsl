<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output version="1.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <feed xmlns="http://www.w3.org/2005/Atom">
            <title>Keep up with your television shows</title>
            <subtitle>Atom feed for keeping up with your television shows.</subtitle>
            <link href="http://localhost:8888" rel="self"/>
            <id>http://localhost:8888/</id>
            <updated>2016-01-01T00:00:00+02:00</updated>

            <xsl:for-each select="shows/show">
            <entry>
                <title><xsl:value-of select="title"/></title>
                <link href="{link}"/>
                <id><xsl:value-of select="link"/></id>
                <updated><xsl:value-of select="released_at"/></updated>
                <summary><xsl:value-of select="description"/></summary>
            </entry>
            </xsl:for-each>
        </feed>
    </xsl:template>
</xsl:stylesheet>

