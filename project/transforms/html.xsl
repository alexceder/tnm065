<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;&#xa;</xsl:text>

        <html lang="en">
            <head>
                <meta charset="utf-8"/>
                <title>Projekt</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/css/bootstrap.min.css"/>
            </head>

            <body>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="text-center">Television shows</h1>

                            <hr/>

                            <xsl:for-each select="shows/show">
                                <h5><xsl:value-of select="title"/></h5>
                                <p><small><xsl:value-of select="released_at"/></small></p>
                                <p><xsl:value-of select="description"/></p>
                                <p class="text-center"><img src="/api/shows/{slug}/image" alt="{title} image"/></p>
                                <p><a href="{link}" title="{link}"><xsl:value-of select="substring(link, 0, 40)"/>..</a></p>
                            </xsl:for-each>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
