<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output version="1.0" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <rss version="2.0">
        <channel>
            <title>Keep up with your television shows</title>
            <link>http://localhost:8888</link>
            <description>Atom feed for keeping up with your television shows.</description>

            <xsl:for-each select="shows/show">
            <item>
                <title><xsl:value-of select="title"/></title>
                <link><xsl:value-of select="link"/></link>
                <guid><xsl:value-of select="link"/></guid>
                <description><xsl:value-of select="description"/></description>
                <pubDate><xsl:value-of select="released_at"/></pubDate>
            </item>
            </xsl:for-each>
        </channel>
        </rss>
    </xsl:template>
</xsl:stylesheet>
