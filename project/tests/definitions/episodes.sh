#!/usr/bin/env sh

basedir=$(dirname $0)

curl -sS localhost:8888/api/episodes?type=raw |
xmllint --noout --dtdvalid $basedir/../../definitions/episodes.dtd -

if [ $? -eq 0 ]; then
    echo "\033[0;32mWell formed\033[0m"
else
    echo "\033[0;31mThere were errors!\033[0m"
fi
