(function () {
'use strict';

// Pages

var Upcoming = Vue.extend({
    template: '#watchlist',

    data: function () {
        return {
            episodes: [],
            title: 'Upcoming'
        }
    },

    ready: function () {
        var auth = this.$resource('/api/auth');

        auth.get().then(function (res) {
            var resource = this.$resource('/api/users/{user}/upcoming');

            window.authenticated = res.data.user.username;

            resource.get({ user: window.authenticated }).then(function (res) {
                this.$set('episodes', res.data.episodes);
            });
        }, function (res) {
            //
        });
    },
});

var Watchlist = Vue.extend({
    template: '#watchlist',

    data: function () {
        return {
            episodes: [],
            toggleable: true,
            title: 'Watchlist'
        }
    },

    computed: {
        numberUnwatched: function() {
            return this.episodes.filter(function (episode) {
                return !episode.watched;
            }).length;
        }
    },

    ready: function () {
        var auth = this.$resource('/api/auth');

        auth.get().then(function (res) {
            var resource = this.$resource('/api/users/{user}/watchlist');

            window.authenticated = res.data.user.username;

            resource.get({ user: window.authenticated }).then(function (res) {
                this.$set('episodes', res.data.episodes);
            });
        }, function (res) {
            //
        });
    },

    methods: {
        toggleWatched: function (index) {
            var episode = this.episodes[index],
                resource;

            episode.watched = !episode.watched;

            if (episode.watched) {
                resource = this.$resource('/api/users/{user}/watchlist/{id}');
                resource.delete({
                    user: window.authenticated,
                    id: episode.id
                }).then(function (res) {
                    //
                });
            } else {
                resource = this.$resource('/api/users/{user}/watchlist');
                resource.save({
                    user: window.authenticated
                }, {
                    episode_id: episode.id
                }).then(function (res) {
                    //
                });
            }
        }
    }
});

var Shows = Vue.extend({
    template: '#shows',

    data: function () {
        return {
            shows: []
        }
    },

    ready: function () {
        var auth = this.$resource('/api/auth');

        auth.get().then(function (res) {
            var resource = this.$resource('/api/users/{user}/shows');

            window.authenticated = res.data.user.username;

            resource.get({ user: window.authenticated }).then(function (res) {
                this.$set('shows', res.data.shows);
            });
        }, function (res) {
            var resource = this.$resource('/api/shows{/id}');

            resource.get().then(function (res) {
                this.$set('shows', res.data.shows);
            });
        });
    },

    methods: {
        toggleSubscribed: function (index) {
            var show = this.shows[index],
                resource;

            if (show.subscribed) {
                resource = this.$resource('/api/users/{user}/subscriptions/{id}');
                resource.delete({
                    user: window.authenticated,
                    id: show.id
                }).then(function (res) {
                    show.subscribed = false;
                }, function () {});
            } else {
                resource = this.$resource('/api/users/{user}/subscriptions');
                resource.save({
                    user: window.authenticated
                }, {
                    show_id: show.id
                }).then(function (res) {
                    show.subscribed = true;
                }, function () {});
            }

        }
    }
});

var ShowsNew = Vue.extend({
    template: '#shows-new',

    data: function () {
        return {
            form: {
                title: null,
                released_at: null,
                description: null,
            }
        }
    },

    methods: {
        create: function (index) {
            var resource = this.$resource('/api/shows');
            resource.save({}, this.form).then(function (res) {
                this.$route.router.go({
                    name: 'show',
                    params: {
                        show: res.data.slug
                    }
                });
            });
        }
    }
});

var ShowsShow = Vue.extend({
    template: '#shows-show',

    data: function () {
        return {
            show: {
                title: null,
                released_at: null,
                description: null,
            },
            episodes: null
        }
    },

    ready: function () {
        var resource;

        resource = this.$resource('/api/shows{/show}');
        resource.get(this.$route.params).then(function (res) {
            this.$set('show', res.data.shows.show);
        });

        resource = this.$resource('/api/shows/{show}/episodes');
        resource.get(this.$route.params).then(function (res) {
            this.$set('episodes', res.data.episodes);
        });
    },
});

var NewShowEpisode = Vue.extend({
    template: '#shows-show-episodes-new',

    data: function () {
        return {
            form: {
                title: null,
                released_at: null,
                season: null,
                episode: null,
                description: null,
            },
            show: null
        }
    },

    ready: function () {
        var resource = this.$resource('/api/shows{/show}');
        resource.get(this.$route.params).then(function (res) {
            this.$set('show', res.data.shows.show);
        });
    },

    methods: {
        create: function (index) {
            var resource = this.$resource('/api/shows/{show}/episodes');
            this.form.show = this.$route.params.show;
            console.log(this.form.show);
            resource.save(this.$route.params, this.form).then(function (res) {
                this.$route.router.go({
                    name: 'show',
                    params: this.$route.params
                });
            });
        }
    }
});


// Components

var User = Vue.extend({
    template: '{{ authenticated }}',

    data: function () {
        return {
            authenticated: null
        }
    },

    ready: function () {
        var resource = this.$resource('/api/auth');

        resource.get().then(function (res) {
            this.$set('authenticated', res.data.user.username);
            window.authenticated = res.data.user.username;
        }, function (res) {
            //
        });
    }
});

Vue.component('auth-user', User);


// App & Router

var App = Vue.extend();

var Router = new VueRouter(/*{ history: true }*/);

Router.map({
    '/': {
        component: Watchlist
    },

    '/upcoming': {
        component: Upcoming,

    },

    '/shows': {
        component: Shows,
    },

    '/shows/new': {
        component: ShowsNew,
    },

    '/shows/:show': {
        name: 'show',
        component: ShowsShow,
    },

    '/shows/:show/episodes/new': {
        name: 'newShowEpisode',
        component: NewShowEpisode,
    },
});

Router.start(App, '#app');


// Helper

Vue.filter('from_now', function (value) {
    return moment(value).fromNow();
});

})();
